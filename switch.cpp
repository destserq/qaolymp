#include "switch.h"
#include "ui_switch.h"
#include <QProcess>
#include <QMessageBox>
#include <INIReader.h>
#include <QFile>

bool Switch::isDeviceEnabled = false;
Switch* Switch::instance = nullptr;

Switch::Switch(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Switch)
{
    if (!instance)
        instance = this;

    ui->setupUi(this);
    //setFixedSize(geometry().width(), geometry().height()); // disabled for being bug
    ui->powerButton->setStyleSheet("border-image:url(:/custom-icons/power-button.png);");

    // Icons for ports 1-6
    labelObj[1] = ui->p1Icon;
    labelObj[2] = ui->p2Icon;
    labelObj[3] = ui->p3Icon;
    labelObj[4] = ui->p4Icon;
    labelObj[5] = ui->p5Icon;
    labelObj[6] = ui->p6Icon;

    // Icons for power and console icon
    labelObj[7] = ui->powerIcon;
    labelObj[8] = ui->consoleIcon;

    // filling std::map of actions in top menu
    actionObj[1] = ui->p1;
    actionObj[2] = ui->p2;
    actionObj[3] = ui->p3;
    actionObj[4] = ui->p4;
    actionObj[5] = ui->p5;
    actionObj[6] = ui->p6;

    sSwitch->initConfig();

    if (!QFile::exists(QCoreApplication::applicationDirPath() + "/switch.ini")) {
        QMessageBox messageBox;
        messageBox.critical(0, "Ошибка", "Отсутствует конфигурационный файл, работа с программой не может быть продолжена!");
        messageBox.setFixedSize(500, 200);
        messageBox.setWindowIcon(QIcon(":/custom-icons/nc-logo.png"));
    }
}

Switch::~Switch()
{
    delete ui;
}

void Switch::initConfig()
{
    QString cfg_name = QCoreApplication::applicationDirPath() + "/switch.ini";
    INIReader reader(cfg_name.toStdString());

    // Getting values from config file
    for (int i = 1; i <= 6; i++) {
        portsEnabled[i] = reader.GetBoolean("port" + std::to_string(i), "enable", false);
        portsNames[i] = reader.GetString("port" + std::to_string(i), "name", "WS" + std::to_string(i));
        if (i != 5) { // defect for fifth port - it's bandwidth cannot be changed
            portsBand[i] = reader.GetString("port" + std::to_string(i), "bandwidth", "100full");
        } else {
            portsBand[i] = "100full";
        }
        portsVLAN[i] = reader.GetInteger("port" + std::to_string(i), "vlan", 1);
    }

    // setting dynamic text to QActions
    ui->p1->setText(QString::fromStdString("Порт 1") + " ("+QString::fromStdString(portsNames.find(1)->second)+")");
    ui->p2->setText(QString::fromStdString("Порт 2") + " ("+QString::fromStdString(portsNames.find(2)->second)+")");
    ui->p3->setText(QString::fromStdString("Порт 3") + " ("+QString::fromStdString(portsNames.find(3)->second)+")");
    ui->p4->setText(QString::fromStdString("Порт 4") + " ("+QString::fromStdString(portsNames.find(4)->second)+")");
    ui->p5->setText(QString::fromStdString("Порт 5") + " ("+QString::fromStdString(portsNames.find(5)->second)+")");
    ui->p6->setText(QString::fromStdString("Порт 6") + " ("+QString::fromStdString(portsNames.find(6)->second)+")");
}

QString Switch::startPing(int srcPort, int destPort)
{
    int srcVLAN = portsVLAN[srcPort]; int destVLAN = portsVLAN[destPort];
    bool srcEnabled = portsEnabled[srcPort]; bool destEnabled = portsEnabled[destPort];
    bool srcAction = actionObj[srcPort]->isChecked(); bool destAction = actionObj[destPort]->isChecked();
    std::string srcBand = portsBand[srcPort]; std::string destBand = portsBand[destPort];

    if (srcVLAN == destVLAN && srcEnabled == true && destEnabled == true
            && srcAction == true && destAction == true
            && srcBand == destBand)
        return "SENT";
    else if (srcVLAN != destVLAN && srcEnabled == true && destEnabled == true
             && srcAction == true && destAction == true
             && srcBand == destBand)
        return "DROPPED";
    return "LOST";
}

void Switch::openConfig()
{
    QProcess *proc = new QProcess(this);
    proc->start("notepad.exe " + QCoreApplication::applicationDirPath() + "/switch.ini");
}

void Switch::changeEvent(QEvent *event) // event that handles hiding of window
{
   if (event->type() == QEvent::WindowStateChange)
   {
       if (isMinimized() == true) {
           for (int i = 1; i <= 6; i++) {
               changeLED(labelObj[i], false);
           }
       }
   }

   return QMainWindow::changeEvent(event);
}

template<class T>
void Switch::changeLED(T object, bool enable) {
    if (isDeviceEnabled == true) {
        if (enable == true && object != ui->powerIcon) {
            for (std::map<int, QLabel*>::iterator it = labelObj.begin(); it != labelObj.end(); ++it) {
                if (it->second == object) {
                    std::string band = portsBand.find(it->first)->second;
                    if (band == "100full") {
                        object->setPixmap(QPixmap(":/custom-icons/green-icon.png"));
                    } else if (band == "10full") {
                        object->setPixmap(QPixmap(":/custom-icons/red-icon.png"));
                    }
                }
            }
        } else if (enable == true) { // this case is created for power button
            object->setPixmap(QPixmap(":/custom-icons/green-icon.png"));
        }
        else if (enable == false) {
            object->setPixmap(QPixmap(":/custom-icons/grey-icon.png"));
        }
    }
    return;
}

void Switch::on_powerButton_toggled(bool checked)
{
    if (checked == false) {
        for (int i = 1; i <= 6; i++) {
            changeLED(labelObj[i], false);
        }
        changeLED(labelObj[7], false); // disable lightning for power icon
        isDeviceEnabled = false;
    }
    else if (checked == true) {
        isDeviceEnabled = true;
        for (int i = 1; i <= 6; i++) {
            if (actionObj[i]->isChecked() && portsEnabled[i] == true && i != 1) // i can't be equal 1 in order to be a bug
                changeLED(labelObj[i], true);
        }
        changeLED(ui->powerIcon, true);
    }
}

void Switch::on_openconfig_triggered(bool /*checked*/)
{
    openConfig();
}

void Switch::on_ping_triggered(bool /*checked*/)
{
    pingWindow->show();
}

void Switch::on_savesettings_triggered(bool /*checked*/)
{
    if (isDeviceEnabled == false) {
        QMessageBox messageBox;
        messageBox.critical(0, "Ошибка", "Устройство отключено, операция отменена.");
        messageBox.setFixedSize(500, 200);
        messageBox.setWindowIcon(QIcon(":/custom-icons/nc-logo.png"));
        return;
    }
    authWindow->show();
}

void Switch::on_p1_toggled(bool /*checked*/)
{
    /*
    if (checked == true) {
        changeLED(ui->p1Icon, true);
    }
    else if (checked == false) {
        changeLED(ui->p1Icon, false);
    }
    */
}

void Switch::on_p2_toggled(bool checked)
{
    if (portsEnabled[2] == false) {
        ui->p2->setChecked(false);
        return;
    }

    if (checked == true) {
        changeLED(ui->p2Icon, true);
    }
    else if (checked == false) {
        changeLED(ui->p2Icon, false);
    }
    if (ui->p1->isChecked() && ui->p3->isChecked() && ui->p4->isChecked()
            && ui->p5->isChecked() && ui->p6->isChecked()) {
        changeLED(ui->p1Icon, true);
    }
}

void Switch::on_p3_toggled(bool checked)
{
    if (portsEnabled[3] == false) {
        ui->p3->setChecked(false);
        return;
    }

    if (checked == true) {
        changeLED(ui->p3Icon, true);
    }
    else if (checked == false) {
        changeLED(ui->p3Icon, false);
    }
    if (ui->p1->isChecked() && ui->p2->isChecked() && ui->p4->isChecked()
            && ui->p5->isChecked() && ui->p6->isChecked()) {
        changeLED(ui->p1Icon, true);
    }
}

void Switch::on_p4_toggled(bool checked)
{
    if (portsEnabled[4] == false) {
        ui->p4->setChecked(false);
        return;
    }

    if (checked == true) {
        changeLED(ui->p4Icon, true);
    }
    else if (checked == false) {
        changeLED(ui->p4Icon, false);
    }
    if (ui->p1->isChecked() && ui->p2->isChecked() && ui->p3->isChecked()
            && ui->p5->isChecked() && ui->p6->isChecked()) {
        changeLED(ui->p1Icon, true);
    }
}

void Switch::on_p5_toggled(bool checked)
{
    if (portsEnabled[5] == false) {
        ui->p5->setChecked(false);
        return;
    }

    if (checked == true) {
        changeLED(ui->p5Icon, true);
    }
    else if (checked == false) {
        changeLED(ui->p5Icon, false);
    }
    if (ui->p1->isChecked() && ui->p2->isChecked() && ui->p3->isChecked()
            && ui->p4->isChecked() && ui->p6->isChecked()) {
        changeLED(ui->p1Icon, true);
    }
}

void Switch::on_p6_toggled(bool checked)
{
    if (portsEnabled[6] == false) {
        ui->p6->setChecked(false);
        return;
    }

    if (checked == true) {
        changeLED(ui->p6Icon, true);
    }
    else if (checked == false) {
        changeLED(ui->p6Icon, false);
    }
    if (ui->p1->isChecked() && ui->p2->isChecked() && ui->p3->isChecked()
            && ui->p4->isChecked() && ui->p5->isChecked()) {
        changeLED(ui->p1Icon, true);
    }
}
