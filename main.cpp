#include "switch.h"
#include <QApplication>
#include "INIReader.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon("./custom-icons/nc-logo.png"));
    Switch w;
    w.show();
    return a.exec();
}
