#ifndef PING_H
#define PING_H

#include <QDialog>

namespace Ui {
class Ping;
}

class Ping : public QDialog
{
    Q_OBJECT

public:
    explicit Ping(QWidget *parent = nullptr);
    ~Ping();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Ping *ui;
};

#endif // PING_H
