#include "ping.h"
#include "ui_ping.h"
#include <switch.h>
#include <QDebug>

Ping::Ping(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Ping)
{
    ui->setupUi(this);
    QIcon icon = windowIcon();
    Qt::WindowFlags flags = windowFlags();
    Qt::WindowFlags helpFlag = Qt::WindowContextHelpButtonHint;

    flags = flags & (~helpFlag);
    setWindowFlags(flags);
    setWindowIcon(icon);

    for (int i = 1; i <= 6; i++) {
        ui->srcBox->addItem(QString::number(i));
        ui->destBox->addItem(QString::number(i));
    }
}

Ping::~Ping()
{
    delete ui;
}

void Ping::on_pushButton_clicked()
{
    int srcPort = ui->srcBox->currentText().toInt();
    int destPort = ui->destBox->currentText().toInt();

    QString res = sSwitch->startPing(srcPort, destPort);
    QString clred;
    if (res == "SENT") {
        clred = tr("<font color='Green'>%1</font>");
    } else if (res == "LOST" || res == "DROPPED") {
        clred = tr("<font color='Red'>%1</font>");
    }
    ui->resultLabel->setText(QString::fromStdString("Результат отправки: ") + clred.arg(res));
}
