#include <string>
#include <QLabel>
#include <INIReader.h>


class Port {
    public:
		int portNum;
		QLabel *portIcon;
		QAction *portAction;

        std::string pName;
        std::string pBand;
        bool isEnabled;
        int pVLAN;

        Port(int i) {
            portNum = i;
            initConfig();
        }

        void initConfig() {
            INIReader reader("switch.ini");
            isEnabled = reader.GetBoolean("Port" + std::to_string(portNum), "enable", true);
            pName = reader.GetString("Port" + std::to_string(portNum), "name", "WS" + std::to_string(portNum));
            pBand = reader.GetString("Port" + std::to_string(portNum), "bandwidth", "100full");
            pVLAN = reader.GetInteger("Port" + std::to_string(portNum), "vlan", 1);
        }
	
};
