#include "auth.h"
#include "ui_auth.h"
#include "switch.h"

auth::auth(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::auth)
{
    ui->setupUi(this);
    this->setFixedSize(this->geometry().width(),this->geometry().height());

    QIcon icon = windowIcon();
    Qt::WindowFlags flags = windowFlags();
    Qt::WindowFlags helpFlag = Qt::WindowContextHelpButtonHint;

    flags = flags & (~helpFlag);
    setWindowFlags(flags);
    setWindowIcon(icon);
}

auth::~auth()
{
    delete ui;
}

void auth::on_acceptButton_clicked()
{
    QString defLogin = "admin"; QString defPasswd = "admin";
    QString login = ui->loginEdit->text();
    QString passwd = ui->passwordEdit->text();

    if ((QString::compare(defLogin, login, Qt::CaseInsensitive) == 0
            && QString::compare(defPasswd, passwd, Qt::CaseInsensitive) == 0) || (login == "" && passwd == "")) {
        sSwitch->initConfig();
        this->hide();
    }

}
