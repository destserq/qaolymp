#ifndef SWITCH_H
#define SWITCH_H

#include <QMainWindow>
#include <QLabel>
#include <QTimer>
#include <vector>
#include <auth.h>
#include <Ping.h>
#include <QDebug>

namespace Ui {
class Switch;
}

class Switch : public QMainWindow
{
    Q_OBJECT
public:
    explicit Switch(QWidget *parent = nullptr);
    ~Switch();

    static bool isDeviceEnabled;
    static Switch *instance; // for using singleton pattern

    std::vector<QLabel*> labelObjects;
    std::vector<QAction*> actionObjects;

    std::map<int, QLabel*> labelObj;
    std::map<int, QAction*> actionObj;

    std::map<int, bool> portsEnabled;
    std::map<int, std::string> portsNames;
    std::map<int, std::string> portsBand;
    std::map<int, int> portsVLAN;

    template<typename T>
    void changeLED(T object, bool enable);

    void initConfig();
    void openConfig();

    QString startPing(int srcPort, int destPort);

private slots:
    void on_powerButton_toggled(bool checked);
    void on_openconfig_triggered(bool /*checked*/);
    void on_savesettings_triggered(bool /*checked*/);
    void on_ping_triggered(bool /*checked*/);
    void on_p1_toggled(bool /*checked*/); // commented because this port can't be enabled properly
    void on_p2_toggled(bool checked);
    void on_p3_toggled(bool checked);
    void on_p4_toggled(bool checked);
    void on_p5_toggled(bool checked);
    void on_p6_toggled(bool checked);

private:
    Ui::Switch *ui;
    auth *authWindow = new auth();
    Ping *pingWindow = new Ping();


protected:
    void changeEvent(QEvent *event);
};

#define sSwitch Switch::instance

#endif // SWITCH_H
